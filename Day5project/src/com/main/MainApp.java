package com.main;

import com.model.Department;
import com.model.Employee;

public class MainApp {

	public static void main(String[] args) {
		Employee employee1 = new Employee(10, "Hello", 1010.10f); //at the time create assigned value  
		
		Employee employee2 = new Employee(20, "World", 2020.20f);
		
		
		
		Department department = new Department(123,"Development");
		department.setEmployee(employee2);
		
		System.out.println(department.getDeptNo());
		
		System.out.println(department.getDeptName());
		System.out.println(department.getEmployee().getEmpNo());
		System.out.println(department.getEmployee().getEmpName());
		System.out.println(department.getEmployee().getSalary());
		
		System.out.println("Bi direction");
		System.out.println(employee2.getEmpNo());
		System.out.println(employee2.getEmpName());
		System.out.println(employee2.getSalary());
		// association 
		employee2.setDepartment(department);
		System.out.println(employee2.getDepartment().getDeptNo());
		System.out.println(employee2.getDepartment().getDeptName());
	}

}
