package com.model;

public class Department {
	private int deptNo;
	private String deptName;
	private Employee   employee   ;  // empNo + empName + salary
			

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	// constructor
	public Department() {
		super();
	}

	public Department(int deptNo, String deptName) {
		super(); // parent class has to be created
		this.deptNo = deptNo;
		this.deptName = deptName;
	}

	

	@Override
	public String toString() {
		return "Department [deptNo=" + deptNo + ", deptName=" + deptName + "]";
	}

	public int getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(int deptNo) {
		this.deptNo = deptNo;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

}
